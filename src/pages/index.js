import React from "react"
import { Link } from "gatsby"

import Layout from "../components/layout"
import Image from "../components/image"
import SEO from "../components/seo"

const IndexPage = () => (
  <Layout>
    <SEO title="Home" />
    <h1>Hi there</h1>
    <p>I'm sure you must've heard about our awesome web design and online marketing deals :)</p>
    <p>Our website is currently under construction. Feel free to send a Whastapp message to <a href='tel:+27719348502'>0719348502</a></p>
    <p>We'll get back to you as soon as possible.</p>
    <br />
    <p><i>Stay Connected</i></p>
    <div style={{ maxWidth: `300px`, marginBottom: `1.45rem` }}>
      <Image />
    </div>
    <Link to="/page-2/">Go to page 2</Link>
  </Layout>
)

export default IndexPage
